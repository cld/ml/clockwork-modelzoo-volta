Models in this repository have been compiled and optimized for MPI-SWS volta cluster machines (tesla v100 with 32GB memory).

[Main Clockwork Repository](https://gitlab.mpi-sws.org/cld/ml/clockwork)

Set the `CLOCKWORK_MODEL_DIR` environment variable to point to this repository checkout on all worker machines.