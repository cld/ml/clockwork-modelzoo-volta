Models with winograd optimizations; increases weights size by ~1.5, decreases execution latency by around a third.
